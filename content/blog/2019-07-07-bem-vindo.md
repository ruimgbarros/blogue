---
title: Bem-vindo,
author: Rui Barros
date: '2014-07-28'
slug: bem-vindo
categories: []
tags:
  - hello world
description: Desc
hacker_news_id: ''
lobsters_id: ''
meta_img: /blog/2019-07-07-bem-vindo_files/ikea_instruções.png
---

Se estiver por cá para ler sobre os grandes assuntos que preocupam a humanidade – como tirar uma boa selfie, a melhor receita para o bolo de chocolate ou como ser hipster em 10 passos – terei de o convidar a deslocar o cursor ao X junto à ao separador desta página e clicar.

**Se forem outras as causas que o trazem por cá, seja bem-vindo.**

Convido o caro leitor a sentar-se num canto e a desfrutar de uma qualquer leitura corriqueira. Esteja à vontade, pode servir-se como bem entender. Encha mais o copo, vá!

Infelizmente mudei-me recentemente para esta morada, por isso poderei não ter muito para oferecer. Aliás, convidei-o a sentar-se quando, na realidade, nem cadeiras tenho. Bem, **terá que montar uma antes de se sentar**. Vá, eu deixo aqui uma ajuda.

![intruções ikea](/blog/2019-07-07-bem-vindo_files/ikea_instruções.png)

**Bem sei que não é tarefa fácil.**

Se fosse, as cadeiras já estariam todas montadas e o leitor teria à sua disposição uma coleção de agradáveis Ivar’s do IKEA, prontas a sentar.

Mas não. Isto é um projeto sem ambição, por isso será sempre um blogue onde terá de montar a sua própria cadeira. Quando eu montar uma, até posso partilhar um tutorial sobre como fazer. Mas não espere muito disto.

Vá, **não estava à espera de uma coisa séria pois não?**

Mas quando terminar a cadeira, avise-me por favor. Encontrará por aí o meu contacto. **Esteja à vontade**. E deixe-me um bocadinho desse Whisky com que se serviu, por favor.

