---
title: Acesso a dados. Guia prático para assessores de imprensa
author: Rui Barros
date: '2019-07-14'
slug: acesso-a-dados-guia-prático-para-assessores-de-imprensa
categories: []
tags: []
best: true
meta_img: blog/2019-07-09-acesso-a-dados-guia-prático-para-assessores-de-imprensa_files/4528443760_4eb724a0db_b.jpg
---

![open data](/blog/2019-07-09-acesso-a-dados-guia-prático-para-assessores-de-imprensa_files/4528443760_4eb724a0db_b.jpg)

Num país que ocupa o [45º lugar no Ranking Global de Dados Abertos](https://index.okfn.org/place/) (ali mesmo ao lado desse farol democrático que é a Turquia), uma grande parte do meu trabalho como jornalista de dados é passado a fazer *scrape* de dados que deveriam estar abertos ou a abordar assessores de imprensa para pedir um excel perdido nos antípodas de um servidor de uma instituição pública.

Sempre que pego no telemóvel para pedir o acesso a certos dados - muitas das vezes os dados até já são públicos, mas estão agregados - a resposta passa sempre por um pedido oficial via e-mail. Para que seja encaminhado “para quem percebe disto”.

Se é verdade que a habitual dança institucional entre jornalistas e assessores passa também por estes pedidos, não consigo deixar de notar uma aparente condescendência de quem entende que os dados devem já chegar agregados e o mais simplificados possível.

“Mas vai ficar afogado em números” ou “tem mesmo a certeza que quer isto tudo?”, são algumas das formulações desta forma de condescendência a que já me habituei.

É certo que o número de jornalistas de dados em Portugal está longe de ser suficiente para os assessores deste país estarem habituadas a este género de pedidos. Mas do lado que quem faz vida como fonte profissional, haverá certamente a noção de que nenhum jornalista gosta da sensação de estar a ser manipulado. É que estes jornalistas leram o ["How to lie with statistics"](https://en.wikipedia.org/wiki/How_to_Lie_with_Statistics).

Costumo dizer que “todos os números mentem, mas alguns mentem mais do que outros”, numa adaptação de um adágio parecido sobre mapas. Dados agregados não só podem mentir como podem impedir o jornalista de chegar ao *outlier* que é a verdadeira história.

<center>
<img class="aside" src="/blog/2019-07-09-acesso-a-dados-guia-prático-para-assessores-de-imprensa_files/how to lie with statistics.jpg" alt="Facebool malvado">
</center>

Bem sei que a pouca abertura para ceder os dados na sua forma menos trabalhada vem da noção empírica de que o seu trabalho passa por “facilitar a vida” ao jornalista, mas quando falamos de jornalistas de dados estamos a falar de profissionais treinados para olhar para dos dados de forma crítica.

Não é uma questão de “coragem” para enfrentar uma folha de excel com milhares de registos. Se os meus colegas forem como eu, há até um certo prazer sádico em ter tantas histórias em potencial por explorar.

Deixem-nos por isso “chafurdar” em dados.

## Uma nota de *nostra culpa*

Quando se fala de transparência e acesso a dados das instituições públicas, o discurso costuma passar pelo tom acusatório às instituições públicas, que não libertam os dados e que assim não há transparência.

É verdade que é preciso tomar medidas urgentes para aumentar a transparência da nossa administração pública, mas é preciso perguntar também que papel o jornalismo deve ter neste processo.

Um [artigo científico](http://www.revistaej.sopcom.pt/ficheiros/20180802-ej8_2018.pdf) de três investigadores da Universidade do Minho mostra que, das 2669 queixas apresentadas à Comissão de Acesso aos Documentos da Administração (CADA) entre 2011 e 2016, apenas 63 (2.4%) foram apresentados por jornalistas.

Ser *watchdog* do poder público implica também responsabilidade. Pedir documentos em vez de fazer diretos cheios de nada poderia ser uma boa iniciativa.

Isso, e claro, [libertá-los](https://shifter.sapo.pt/2019/06/radio-renascenca-jornalismo-de-dados-open-source/). A bem da transparência pública.

## Sinais de progresso

Apesar dos graves problemas já identificados, há sinais de que as coisas podem estar efectivamente a mudar.

Para além do [Dados.gov](https://dados.gov.pt/pt/), as Câmaras de Lisboa, Porto, Cascais e Águeda são pioneiras ao terem o seu próprio portal de dados abertos, o que mostra já alguma sensibilidade do poder local para a questão. A estas autarquias, aqui fica o meu modesto elogio público.

A par destes portais, acrescento o trabalho notável feito pelo pessoal da [Central de Dados](http://centraldedados.pt/). São iniciativas cívicas notáveis que mereciam mais atenção por parte das instituições públicas.

Na **Renascença** mantenho-me fiel ao compromisso de disponibilizar, sempre que possível, os dados e a análise na base dos nossos trabalhos com dados [num repositório](https://gitlab.com/Renascenca/dados).

É um projecto que dá trabalho, mas o *feedback* positivo e o facto de outras publicações até já terem usado os nossos dados faz-me acreditar que vale a pena.

Nota: Para os interessados no tema, aconselho uma vista de olhos pelo [dadosabertos.pt](http://dadosabertos.pt/).




