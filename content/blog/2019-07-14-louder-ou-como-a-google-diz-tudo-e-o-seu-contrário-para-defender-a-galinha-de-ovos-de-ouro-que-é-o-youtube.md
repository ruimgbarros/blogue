---
title: “Louder”. Ou como o YouTube diz tudo e o seu contrário
author: Rui Barros
date: '2019-07-14'
slug: louder-ou-como-a-google-diz-tudo-e-o-seu-contrário
tags: []
description: Desc
hacker_news_id: ''
lobsters_id: ''
meta_img: /blog/2019-07-14-louder-ou-como-a-google-diz-tudo-e-o-seu-contrário-para-defender-a-galinha-de-ovos-de-ouro-que-é-o-youtube_files/christian-wiediger-NmGzVG5Wsg8-unsplash.jpg
---

![Youtube](/blog/2019-07-14-louder-ou-como-a-google-diz-tudo-e-o-seu-contrário-para-defender-a-galinha-de-ovos-de-ouro-que-é-o-youtube_files/christian-wiediger-NmGzVG5Wsg8-unsplash.jpg)
Foto:<a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@christianw?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Christian Wiediger"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Christian Wiediger</span></a>

Toda a tecnológica de Silicon Valley que se preze disse, um dia, que queria “mudar o mundo”. Está escrito nas “Wired” desta vida, nas entrevistas aos CEO’s, nos sites das empresas e nas t-shirts que os seus empregados vestem. Mas o que acontece quando efetivamente mudam o mundo? Estão estas empresas dispostas a assumir as consequências dessas mudanças?

Parte da resposta vem no episódio #145 de “Reply All”. Não. 

<iframe scrolling="no" allow="autoplay" frameborder="no" width="100%" height="130" style="border-radius: 3px; height: 130px; width: 100%; box-shadow: 0 0 25px 0 rgba(0, 0, 0, 0.15);" src="https://player.gimletmedia.com/rnhzlo"></iframe>


As gigantes tecnológicas estão apenas prontas para receber os louros das grandes mudanças que trouxeram à internet. Mas quando chega a hora de assumir as consequências, são piores do que um artista de circo. E dizem tudo e o seu contrário para defenderem a sua principal fonte de rendimento: o tráfego gerado pelos seus utilizadores.

Não me vou alongar muito sobre o contexto. O pessoal da Gimlet conseguiu fazê-lo de forma detalhada equilibrada (algo relativamente difícil de encontrar nos dias de hoje). Mais detalhes podem ser lidos [aqui](https://www.vox.com/recode/2019/6/10/18660364/vox-youtube-code-conference-susan-wojcicki-carlos-maza) ou [aqui](https://www.wired.com/story/youtube-carlos-maza/).

Mas gostava que se focassem na história do algoritmo do YouTube contada por [Kevin Roose](https://twitter.com/kevinroose?lang=pt) neste podcast.

O YouTube encontrou o algoritmo certo para manter os seus utilizadores presos num mundo de sugestões. Esse algoritmo promove ideias radicais e de extrema-direita. Deu um forte contributo à Alt-Right, mas também aos movimentos que defendem o fim das vacinas, da terra plana e outras que tais. Quando chamado à responsabilidade perante uma clara violação dos termos e condições do seu serviço, o YouTube assobia para o lado. Porque, para o YouTube, não há essa coisa chamada “má clientela”.

Desde os tempos do Ray William Johnson que fui acumulando criadores de conteúdo que sigo religiosamente nesse site. Aprendi imensa coisa, descobri bandas incríveis, ri, comovi-me e revoltei-me perante o site que começou com [um tipo no Zoo](https://www.youtube.com/watch?v=jNQXAC9IVRw). O YouTube foi, para mim, a caixinha que mudou o mundo.

Mas depois de ouvir a [resposta oficial do YouTube](https://www.theverge.com/2019/6/4/18653088/youtube-steven-crowder-carlos-maza-harassment-bullying-enforcement-verdict) a este caso só consigo pensar o quanto gostaria de ter um outro site que não fizesse estas manobras de contorcionista. É que este site mudou o mundo. Só não sabe ainda bem quanto. 

E, à boa velha maneira de Silicon Valley, só agora tem real noção do que criou.

<hr>

**Nota 1:** 
O YouTube [anunciou](https://www.theverge.com/2019/7/11/20691123/youtube-creator-harassment-vidcon-carlos-maza-steven-crowder-neal-mohan) no dia 11 deste mês que estava a trabalhar em regras para evitar ‘creator-on-creator harassment’. Mas depois de quase um mês a dizer tudo e o seu contrário, vou esperar para ver.

**Nota 2:** Não, isto não é uma questão de liberdade de expressão. É uma questão de termos e condições e regras que nos foram apresentadas por uma empresa privada quando aceitamos fazer parte da sua comunidade mas que, agora, porque não lhe convém, não quer fazer valer esses termos e condições.