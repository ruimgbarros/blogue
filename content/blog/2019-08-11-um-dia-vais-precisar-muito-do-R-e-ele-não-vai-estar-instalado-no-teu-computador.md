---
title: Um dia vais precisar muito do R e ele não vai estar instalado no teu computador
date: '2019-08-11'
slug: um-dia-vais-precisar-muito-do-R-e-ele-não-vai-estar-instalado-no-teu computador
categories: []
tags: []
description: A história de como fiquei sem R e RStudio porque, resumidamente, sou um idiota
meta_img: /blog/2019-08-11-darkstudio_files/rstudio_error.png
---
Último dia antes das férias. 

A tralha de ficheiros acumulava-se no computador e, como não gosto de deixar a minha vida digital desarrumada, decidi correr aqueles programas da praxe, limpar o desktop, eliminar aquele ficheiro que só fiz download para ver como funciona e, claro, deitar um olho à pasta "Aplicações" para ver se não tinha lá nada que já não usasse (claro que tinha). 

3,2,1... scripts corridos, limpeza feita, paninho passado no ecrã... estava tudo pronto para as férias.

A Galiza estava fenomenal - como sempre, mas talvez fale disso noutro post. Mas claro que não ia conseguir ficar longe do computador durante muito tempo. Uma urgência levou-me a ter que abrir o computador para correr um script de geolocalização que já tinha guardado. 

Coisa rápida, que tomaria, claro, uns 15 minutos. *Cmd + spacebar* para abrir o Spotlight, "Rstudio" para abrir o IDE de eleição e...

![erro rstudio this site can't be reached](/blog/2019-08-11-darkstudio_files/rstudio_error.png)

O hotel não tinha ligação à internet, a ligação 3G era terrível e eu estava sem RStudio por uma razão que desconhecia.

Não importava o quer que tivesse googlado. Nada se parecia com o meu problema. E, quando ninguém no Stack Overflow tem o mesmo problema, sabes que estás completamente lixado.

Mas a sorte protege os audazes e, depois de mil formulações do mesmo problema, lá encontrei este tweet:

<center>

<blockquote class="twitter-tweet" data-lang="pt" data-theme="light"><p lang="en" dir="ltr"><a href="https://twitter.com/rstudiotips?ref_src=twsrc%5Etfw">@rstudiotips</a>: Couldn&#39;t get <a href="https://twitter.com/rstudio?ref_src=twsrc%5Etfw">@Rstudio</a> running today on multiple OSX machines - error 127.0.0.1 refused to connect. Was focused on troubleshooting Rstudio via suggestions at website, but problem was with R. Rstudio dx report helped shift focus &amp; fixed by reinstalling R. <a href="https://twitter.com/hashtag/rstats?src=hash&amp;ref_src=twsrc%5Etfw">#rstats</a></p>&mdash; Will Oldham (@wm_oldham) <a href="https://twitter.com/wm_oldham/status/1142635871746887680?ref_src=twsrc%5Etfw">23 de junho de 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

</center>

Sim. Acertaste. 

Alguém achou por bem **desinstalar o R** do seu computador antes de ir de férias.

<center>
<span style='font-size:3rem'>
🤦🤦🤦
</span>
</center>

## "Always look on the bright side of life"

Com a minha ligação à internet super veloz lá consegui descarregar o R e, depois de montes de erros e coisas que continuavam a não dar, tive que optar peça solução drástica: clean install.

Conclusão: tudo o que era packages, addins e costumizações no meu RStudio foram ao ar, pelo que escrevo este post no meu IDE de sempre, mas que não sinto que é meu.

<center>
<blockquote class="twitter-tweet"><p lang="pt" dir="ltr">Olá, o meu nome é Rui Barros e perdi todos os packages que tinha instalados no R |m/</p>&mdash; Rui Barros (@rui_barros17) <a href="https://twitter.com/rui_barros17/status/1159271067426447360?ref_src=twsrc%5Etfw">August 8, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</center>

Há, no entanto, boas notícias nisto tudo: muito tempo sem ter que instalar o Rstudio do zero levou-me a dar de barato que havia muita coisa que já vinha assim. Não é verdade. E é por isso que, nos próximos tempos, conto vir a produzir um conjunto de posts sobre packages que mais uso nesta linguagem de programação.

O primeiro já está pensado. É sobre o único tema que vale a pena ter instalado no RStudio*.

<span style='font-size:0.6rem'>
*ok, se calhar estou a exagerar um bocadinho, mas garanto que é mesmo bom.
</span>





