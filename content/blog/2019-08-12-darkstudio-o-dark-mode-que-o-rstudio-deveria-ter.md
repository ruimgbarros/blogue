---
title: DaRkStudio. O "Dark Mode" que o RStudio deveria ter
author: ''
date: '2019-08-12'
slug: darkstudio-o-dark-mode-que-o-rstudio-deveria-ter
categories: []
tags: []
description: O Dark Theme do Rstudio não é Dark que chegue para mim. Por isso, e com a ajuda da internet, mudei-o.
meta_img: /blog/2019-08-12-darkstudio-o-dark-mode-que-o-rstudio-deveria-ter_files/DaRkStudio.png
---

Se, como eu, adotas sempre a versão "Dark Mode" de tudo, não deves estar muito feliz com aquele tema...

Não sei se é coisa do meu daltonismo, mas aquele toque azulado sempre me fez achar este tema mesmo muito feio. 

![Dark Theme Default do RStudio](https://support.rstudio.com/hc/article_attachments/115020358727/Screen_Shot_2017-08-24_at_1.22.41_PM.png)

O meu ódio ao Dark Theme do RStudio que não era "dark" que chegue fez-me procurar por alternativas. Acabei por encontrar [um repositório](https://github.com/livelaughriley/daRkStudio) da autoria de Riley Roach chamado **daRkStudio**. 

Na prática é uma modificação do ficheiro .css que controla algumas coisas do design deste IDE.

A partir do código de Riley adaptei um bocadinho mais as coisas - não gostava da forma como ficavam as abas não selecionadas nos painéis - e ficou assim:

![DaRkStudio](/blog/2019-08-12-darkstudio-o-dark-mode-que-o-rstudio-deveria-ter_files/DaRkStudio.png)

A minha versão do código pode ser descarregada [aqui](https://gitlab.com/ruimgbarros/daRkStudio).

Ainda não tive tempo para editar a documentação como deve ser (só precisei de fazer esta alteração por causa da [tragédia das minhas férias](http://blog.ruimgbarros.com/posts/um-dia-vais-precisar-muito-do-r-e-ele-n%C3%A3o-vai-estar-instalado-no-teu-computador/)), mas a instalação é muito simples: é clonar o repositório e substituir os ficheiros de origem que o RStudio instala no vosso computador.

Bem sei que não é o mais importante. Mas já que vou passar horas a olhar para este programa, é bom que goste da forma como o meu código aparece por lá.

É que todos sabemos que ninguém te leva a sério se estiveres a programar com um IDE com fundo branco :).

<center>
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Rare photo of a Discord developer working on light theme <a href="https://t.co/Gz9d4x0oJX">pic.twitter.com/Gz9d4x0oJX</a></p>&mdash; Discord (@discordapp) <a href="https://twitter.com/discordapp/status/974022175379222528?ref_src=twsrc%5Etfw">March 14, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</center>

<hr>

Nota: Este código só modifica os elementos do IDE e não a cor do fundo do editor de código ou a cor do texto. Isso é alterado em *Preferences > Appearence > Editor Theme*. 

No print, estou a usar o "Alice-Improved", um tema também fornecido por Riley Roach [neste repositório](https://github.com/livelaughriley/rsthemes). Se quiseres dar uma de designer, alguém já fez um [editor online para estes temas](https://tmtheme-editor.herokuapp.com/#!/editor/theme/Monokai). 

<hr>

