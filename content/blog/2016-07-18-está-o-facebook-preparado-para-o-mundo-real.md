---
title: Está o Facebook preparado para o mundo real?
author: Rui Barros
date: '2016-07-18'
slug: está-o-facebook-preparado-para-o-mundo-real
categories: []
tags:
  - Facebook
description: Desc
hacker_news_id: ''
lobsters_id: ''
meta_img: /images/image.jpg
---

Não nos enganemos: o Facebook não quer ser palco de notícias – a recente mudança no algoritmo diz precisamente isso. Quer ser um lugar onde as fronteiras entre o público e o privado estão diluídas, colocando o seu utilizador numa redoma de felicidade em que vê as suas crenças e vontades confirmadas numa feed artificial.

Foi para isso que apareceu a funcionalidade Live: para que o utilizador possa mostrar ao “seu mundo”, em direto e sem edição. Transmitir momentos de intimidade e felicidade que arrancam sorrisos ao mesmo tempo que massajam o ego do produtor de conteúdo que, com os números de pessoas que decidiram prestar atenção àquilo que tem para mostrar, sente a sua relevância na rede aumentada.

Mas as ferramentas são aquilo que o utilizador decide fazer com elas. E o que Diamond Reynolds fez [ao contar em direto](https://web.archive.org/web/20160722030908/https://www.facebook.com/100007611243538/videos/1690073837922975/) como o seu noivo tinha acabado de ser baleado por um polícia foi, mesmo que inconscientemente, subverter toda a lógica para que o facebook live foi inventado. A rede social vê-se então atirada para esta chatice que é o mundo real onde conflitos raciais, golpes de estado e violência desenfreada existem.

O facebook não quer estar metido nessa chatice: “nós não estamos no negócio de escolher que assuntos as pessoas devem ler. Estamos no negócio de ligar pessoas a ideias”, escreve Adam Mosseri, gestor de produto, no [blogue do Facebook Newsroom](https://web.archive.org/web/20160808030245/http://newsroom.fb.com/news/2016/06/building-a-better-news-feed-for-you). Mas ao que parece que os utilizadores até querem.

Como descobri com a reportagem [“Instantes de Idlib“](https://web.archive.org/web/20160806151139/http://comumonline.com/instantesdeidlib/), nem sempre é fácil que o mundo verdadeiro ultrapasse a bolha do que se espera que uma rede social seja. Mas quando o faz, o valor jornalístico é incontestável. O caso Diamond Reynolds é só mais um exemplo disso.

Não sei é se o facebook está preparado para isso.

**Nota**: Para os interessados nos streams que o Facebook Live permite, neste link pode ver um mapa com todos os diretos a acontecer de momento. Quando os olhos não puderem ver uma revolução a acontecer, pode ser que o telemóvel de outro possa ajudar (e nunca substituir) no processo de construção noticiosa.