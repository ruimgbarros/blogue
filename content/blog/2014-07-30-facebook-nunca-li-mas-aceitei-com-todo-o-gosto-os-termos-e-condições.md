---
title: Nunca li, mas aceitei com todo o gosto os Termos e Condições
author: Rui Barros
date: '2014-07-30'
slug: facebook-nunca-li-mas-aceitei-com-todo-o-gosto-os-termos-e-condições
categories: []
tags: [
    Facebook
]
description: Desc
hacker_news_id: ''
lobsters_id: ''
meta_img: /images/image.jpg
---
Se o leitor tiver uma conta na rede social [Facebook](https://www.facebook.com/) acredito que nos últimos dias leu **uma notícia que o indignou profundamente**. Era tal a indignação que imagino o leitor a partilhar, cheio de raiva, a ligação no seu mural. E vociferou insultuosos atributos sobre a mãe de Mark Zuckerberg, coitada.

**Tudo porque nunca leu, mas aceitou com todo o gosto os Termos e Condições desta rede social**.

<center>
<img class="aside" src="/blog/2014-07-30-facebook-nunca-li-mas-aceitei-com-todo-o-gosto-os-termos-e-condições_files/facebook_evil.png" alt="Facebool malvado">
</center>

<hr>

Se o leitor tiver uma conta na rede sócia Facebook acredito que nos últimos dias leu uma notícia que o indignou profundamente. Era tal a indignação que imagino o leitor a partilhar, cheio de raiva, a ligação no seu mural. E vociferou insultuosos atributos sobre a mãe de Mark Zuckerberg, coitada.

Tudo porque nunca leu, mas aceitou com todo o gosto os Termos e Condições desta rede social.

## Do que estou a falar:
Se fizer parte do grupo de pessoas que:

- não tem amigos que se indignam com as cenas do Mark,
- não segue nenhuma página noticiosa e o seu feed só tem bebés e gatinhos ou fotos daquela colega de trabalho de bikini,
- não tem conta nesta rede social e não sabe do que falo,

aqui segue um breve resumo do **caso polémico destes dias** que tem servido para voltar a tirar o 1984 da estante e duvidar da bondade da rede da barra azul:

A rede social Facebook publicou **esta semana** um estudo no [site da PNAS](https://www.pnas.org/content/111/24/8788.full) (Proceedings of the National Academy of Sciences of the United States of America) sobre o “contágio emocional” dos utilizadores da sua rede.

## Como?

Adam D. I. Kramer, um dos autores, [explica](https://www.facebook.com/akramer/posts/10152987150867796):

> “A nossa pesquisa procurou investigar esta questão ao retirar prioridade de forma minimal a uma pequena percentagem de conteúdos no Feed de Notícias (com base em palavras emocionais nas publicações), para um grupo de pessoas (cerca de 0,04% dos utilizadores, ou 1 em cada 2500), por um curto período de tempo (uma semana, no início de 2012).”

Por outras palavras, o que a equipa de Kramer fez, com a ajuda da empresa detentora desta rede social, foi mudar “um pouco” o [algoritmo que faz com que as publicações surjam no seu feed de notícias](https://tctechcrunch2011.files.wordpress.com/2014/04/facebook-news-feed-edgerank-algorithm.jpg?w=1279&h=727&crop=1). No fundo os investigadores queriam saber se o utilizador desta rede é contagiado emocionalmente por aquilo que os seus amigos escrevem.

Categorizando posts como **“Positivos”** ou **“Negativos”**, os investigadores fizeram com que 700 mil utilizadores fossem expostos a esta alteração e procuraram saber se eram influenciados pelo “positivismo” ou “negativismo” dos seus amigos.

**E concluíram que sim.**

## Troquemos isto por miúdos:

O *Facebook.com* queria saber se devia continuar a **massajar-lhe o ego** e a mostrar-lhe aquilo que quer ver em vez da realidade.

E ninguém quer vir para o facebook ver coisas más. Se o facebook.com continuar a mostrar algo que não faça os seus utilizadores sorrir, sonhar e sentirem-se felizes por lá estarem, lá se vai a **receita toda em publicidade**.

**O Mark percebeu que a realidade é chata**. E todos queremos habitar um mundo perfeito, mesmo que esse só exista debaixo daquela barra azul.

Depois de nos dar só aquilo que nós queremos saber sobre as pessoas com quem mais interagimos na rede o próximo passo é óbvio: **só dar aquilo que nos faz feliz**.

E, caramba, como estar no facebook vai ser bom…

## Validade Científica do Estudo

![Calvin e Hobbes - All we have is one fact you made up](/blog/2014-07-30-facebook-nunca-li-mas-aceitei-com-todo-o-gosto-os-termos-e-condições_files/1366172577.jpg)

Se poderíamos reprovar eticamente o estudo levado a cabo com os utilizadores do facebook.com – e foram bastantes os que o fizeram – a grande questão está na **reprovação da validade cientifica deste estudo**.

**Porquê?**

A ferramenta para analisar e categorizar os posts como “positivos” e “negativos” foi o [Linguistic Inquiry and Word Count (LIWC) 2007](http://www.liwc.net/), um software de análise de texto. Ora, o que há de errado com esta ferramenta?

**É que a linguagem humana é um pouco mais complexa do que isto** e não é assim tão facilmente analisada por um software, como explica John Grohl, fundador do site Psych Central.

Vejamos os seguintes casos (em inglês, já que o estudo foi conduzido pelos utilizadores que publicam na rede nessa língua)

- I’m not happy (Não estou feliz)
- I’m not having a great day (Não estou a ter lá grande dia)

Segundo a metodologia do estudo, estas duas frases, **claramente “negativas”**, seriam categorizadas como **“positivas”**, já que o software utilizado caracteriza a palavra “happy” e “great” como sendo positivas.

Um erro que não é tido em conta pelos investigadores no estudo publicado e que claramente **retira alguma da validade científica** que o estudo reclama ter.

<hr>

# Revolta para quê? Vocês concordaram!

Se há uma coisa que o caso Snowden nos ensinou foi que ninguém gosta de se sentir manipulado. Não entrarei pelas duvidosas questões éticas que o estudo levanta. Penso que é óbvio que ninguém gosta de sentir que fez parte de uma experiência sem ter sido informado.

Para além disso, podiamos abordar a probabilidade do facebook.com começar a ser utilizado como uma arma de manipualção massiva.

**Mas terá o facebook.com agido contra a vontade “legal” dos seus utilizares ao serem envolvidos neste estudo?**

**A resposta é fácil: não.**

Ao inscrever-se e ao concordar com os termos e condições do facebook.com o **utilizador autoriza a empresa a recolher dados sobre si** e autoriza que esses dados sejam utilizados a fim de “melhorarem o seu serviço”

E fazer o nosso feed de notícias mais **feliz** faz parte da política do facebook.com para “melhorar o seu serviço”.

Nunca ninguém lê os termos e condições de um serviço.

Estará na hora de começar a ler?

