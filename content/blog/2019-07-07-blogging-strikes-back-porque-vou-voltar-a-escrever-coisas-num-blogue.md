---
title: Blogging Strikes Back. Porque vou voltar a escrever coisas num blogue
author: Rui Barros
date: '2019-07-07'
slug: blogging-strikes-back-porque-vou-voltar-a-escrever-coisas-num-blogue
categories: []
tags:
  - 
description: Desc
best: true
meta_img: /blog/2019-07-07-blogging-strikes-back-porque-vou-voltar-a-escrever-coisas-num-blogue_files/uploads%2Fcard%2Fimage%2F679467%2F6d2716cb-6e4a-4bd0-b8e0-4366c57b17a4.jpg%2F950x534__filters%3Aquality%2880%29.jpg
---


Ciclo da vida dos meus blogues: Durante uma/duas semanas dedico várias horas a preparar o seu nascimento - a veia *geek* obriga a pensar o CMS, a adaptar um tema, etc, etc - escrevo por um ou dois meses e, depois disso, o vazio total. 

Não sei porque é que não sou capaz de dedicar tempo a isso - talvez seja mesmo só um desastre a comprometer-me com projectos. Mas, em 2019, quatro anos depois de ter convidado os meus leitores a [montar uma cadeira do IKEA](/posts/bem-vindo/), estou de volta a esta atividade.

A culpa é do [Mário Rui André](https://mruiandre.com/2019/06/porque-um-blogue/), que parece ter iniciado um [movimento migratório em massa](https://twitter.com/mruiandre/status/1146535514411081729) para estes espaços:

> Ora, porquê um blogue em 2019? Por uma Internet com tempo para escrever e pensar, por uma Internet menos agitada por polémicas, mais humilde. Por uma Internet menos centralizada em plataformas que sugam dados a torto e a direito, e nos fecham cada vez mais nos seus ecossistemas – mundos onde algoritmos definem o que vemos e o que podemos partilhar. (...) 

>**Há uma net demasiado fixe para se perder tempo a alimentar feeds de redes sociais.**

As palavras do Mário fizeram-me lembrar uma outra Internet. Uma Internet em que o o MSN Messenger era a agenda de contactos que se tinha quase sempre aberta, mas que mais nenhum site era constante definida na lista de tabs abertas. E onde os blogues, os fórums e as Wikis eram o centro de um ecossistema descentralizado de conhecimento e criatividade.

Não, isto não é um post de um saudosista. Essa internet também tinha problemas. Mas tudo o que lá lia não vivia preso dentro de duas bolhas algorítmicas. Sei que esta nova Internet, mais social e mais centralizada em plataformas, tem vantagens. Por exemplo, trouxe para a rede pessoas que não se sentiam à vontade com feeds de RSS.

Mas pagamos um preço demasiado alto por isso.

## Declaração de independência

Ao contrário dos mil blogues que comecei, este tem uma novidade. Foi todo produzido usando um package de R chamado [Blogdown](https://bookdown.org/yihui/blogdown/).

Significa isto que, aqui, conheço cada uma das linhas de código que estão a gerar este site e controlo o servidor onde isto está. A estética minimalista serve precisamente o propósito: terão texto, multimédia, reflexões e pouco mais.

Ah, e não há caixa de comentários. Talvez venha a implementar isso num futuro próximo - se me apetecer.

Este desafio trouxe alguns senãos. O meu blogue será alimentado usando o Rstudio, por exemplo, que apesar de ser o meu IDE preferido, não é propriamente o primeiro sítio em que pensaria escrever um post.

## O que vou poder ler por cá?

Isto é um blogue de um jornalista de dados. E, portanto, acredito que vou falar sobre as linguagens de programação que mais uso, sobre open data, e, claro, jornalismo digital.

Como foi a ler coisas escritas em blogues que aprendi imensa coisa sobre estatística, análise de dados e programação, espero recorrer a este espaço para partilhar uma ou outras coisas que aprendi - e que, acredito, podem ser úteis a alguém.

Mas não desesperes. Vou escrever muitos disparates e coisas inúteis - afinal, isto ainda é um espaço na internet.

Há quatro anos convidei-te, com desdém, a montar uma cadeira do IKEA se quisesses ficar por cá. O repto continua. O espaço e, espero, a periodicidade, é que serão diferentes.


