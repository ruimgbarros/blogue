+++
author = "Rui Barros"
best = false
categories = []
date = 2020-01-14T00:00:00Z
meta_img = "/uploads/tf-idf.png"
slug = ""
tags = []
title = "O que o google diz sobre o meu último ano"

+++
Dizem que somos as perguntas que fazemos. Como jornalista que passa a vida a fazer perguntas a dados, não podia concordar mais com esta ideia. Com o ano de 2019 a fechar, peguei precisamente nesse velho adágio para me perguntar: o que é que as perguntas que fiz em 2019 dizem sobre mim?

Foi assim que pedi à Google todas as pesquisas que fiz no último ano para tentar fazer uma retrospetiva do ano. Como sempre, [deixo o código](https://gitlab.com/ruimgbarros/dados-google-2019) (sem o meu histórico de pesquisa 😅) para quem quiser fazer uma coisa parecida.

***

## Diz-me o que pesquisaste e eu digo-te o que fizeste

Se somos o que pesquisamos, então as palavras mais relevantes de cada mês ajudarão a fazer uma retrospetiva do que foi o meu ano.

Calculei o **tf–idf** das palavras em cada mês para perceber a relevância que uma palavra teve em determinado mês (quando comparada com a frequência dessa palavra ao longo do ano). Pensemos nesta métrica como sendo a palavra que é uma autêntica "impressão digital" daquele mês.

![](/uploads/tf-idf.png)

O ano arrancou com um interesse muito grande no Obama - [assinei um trabalho](https://rr.sapo.pt/2019/01/20/mundo/10yearchallenge-de-obama-a-trump-o-que-mudou-na-america/noticia/138004/) sobre 10 anos desde a tomada de posse do ex-Presidente dos EUA - mas também em EPE's - estava a começar a [investigação sobre o negócio dos médicos tarefeiros](https://rr.sapo.pt/2019/06/19/pais/sete-coisas-que-aprendemos-sobre-os-medicos-tarefeiros-no-sns/especial/154922/), que viria só a sair em junho (sim, quando falarem da sustentabilidade do jornalismo, pensem nisto).

Em fevereiro estava muito interessado em fazer o meu primeiro exercício de [scrollytelling](https://www.notion.so/O-que-o-google-diz-sobre-o-que-foi-o-meu-ltimo-ano-47a64f736b8f45ae9724a24b18589f54) com os dados do Ranking das Escolas e em crowdfunding (lol). Foi também o mês em que me meti a tentar aprender neo4j para outro trabalho que acabou por morrer. E não, não comprei nenhum produto Asus 🙃.

Maio é um mês interessante dado o meu interesse em duas coisas: métricas de facilidade de leitura para o trabalho sobre a [legibilidade dos programas para as europeias](https://rr.sapo.pt/multimedia/152006/o-que-dizem-os-programas-dos-partidos-para-as-europeias-confia-em-mim-mas-nao-me-leias) e Game of Thrones ( 🤷‍♂️).

O verão tem uns resultados estranhos. Em Junho morreu Agustina e eu por alguma razão pesquisei mais do que o normal sobre ela e em Julho nota-se um bocadinho que a minha preocupação foi mapas - de todo o tipo.

Setembro estive focado nas legislativas e no [meu amigo Kalman](https://rr.sapo.pt/sondagens/) e em topic modeling. E outubro foi o mês em que fiz o possível e impossível para para aprender Vue.js para a [Calculadora de Custo de Vida](https://rr.sapo.pt/calculadora-custo-de-vida/) (e nota-se um bocado que a minha vida foi só mesmo aquilo).

Em novembro vamos simplesmente associar o 'õ' como algum erro no processamento dos dados e em dezembro parece que o meu interesse sobre jovens ativistas e o desporto rei em 'american' ficou maior.

## 🚨 Alerta 'workaholic'

No último ano fiz alguns dos trabalhos como jornalista de dados que mais me orgulho. E ainda que saiba que certamente não estive a googlar quando saí com os amigos para uma cerveja ou a ver um filme com a namorada, os dados não me deixam mentir: de todas as palavras que ali aparece, só Game of Thrones não está diretamente relacionado com trabalho.

Os dados dizem que sim, eu ainda consigo ir de férias...

_(não parece difícil de ver quando é que fui)_

![](/uploads/pesquisas_mês.png)

E que sim, parece que a minha produtividade (pelo menos em termos de pesquisas) cai um pouco à sexta-feira...

_(em minha defesa, caso alguém da administração da RR esteja a ler, eu faço muitos fins de semana_ 😇)

![](/uploads/pesquisas_dia.png)

Mas os números não são muito simpáticos sobre a forma como giro o meu tempo.

É especialmente interessante o meu "regresso à atividade moderada" ali depois da hora de regresso a Braga e do jantar - tenho a mania do "termino isto depois em casa" - mas também o aumento de atividade ao domingo à noite.

É verdade que este último deve-se ao facto de aproveitar os domingos à noite para ler os RSS, newsletters e coisas que fui guardando, mas na prática isto significa que a minha semana de trabalho começa domingo às 20h00.

Mais preocupante do que isso são as minhas incursões nocturnas pelo Google:

![](/uploads/pesquisas por mês.png)

Só em Junho e em Março é que não registei qualquer pesquisa depois da uma da manhã

_(para minha defesa, as pesquisas feitas às 4h00 da manhã são de coisas como "vento braga")_

## Certo, mas e agora?

Meio do primeiro mês de 2020 não será certamente a melhor altura para falar de resoluções - nem sou gajo para isso.

Apesar da gripe que me está a matar e uma escolha difícil que me anda a tirar o sono, a mesma vontade está cá: quero continuar a fazer coisas com dados e código e, se possível, que isso seja jornalismo. Há tanto por fazer e eu sei que, para o fazer, não poderei impor-me um horário 9-5.

Continuo a [subscrever as palavras de Ira Glass](https://www.youtube.com/watch?v=PbC4gqZGPSY), mas de forma a poder continuar a fazer acontecer, vou ter que aprender a desligar mais.

Daqui a um ano farei as contas a isto.