---
title: “Louder”, ou como a Google diz tudo e o seu contrário para defender a galinha de ovos de ouro que é o YouTube
author: Rui Barros
date: '2019-07-14'
slug: 
categories: []
tags: []
meta_img: 
---

# “Louder”, ou como a Google diz tudo e o seu contrário para defender a galinha de ovos de ouro que é o YouTube

Toda a tecnológica de Silicon Valley que se preze quis, um dia, “mudar o mundo”. Está escrito nas “Wired” desta vida, nas entrevistas aos CEO’s, nos sites das empresas, nas t-shirts que os seus empregados vestem. Mas o que acontece quando efetivamente mudam o mundo? Estão estas empresas dispostas a assumir as consequências dessas mudanças?

Parte da resposta vem no episódio #145 de “Reply All”. Não. 

As gigantes tecnológicas estão apenas prontas para receber os louros das grandes mudanças que trouxeram à internet. Mas quando chega a hora de assumir as consequências a questão muda de figura.

<iframe scrolling="no" allow="autoplay" frameborder="no" width="100%" height="130" style="border-radius: 3px; height: 130px; width: 100%; box-shadow: 0 0 25px 0 rgba(0, 0, 0, 0.15);" src="https://player.gimletmedia.com/rnhzlo"></iframe>

Não me vou alongar muito sobre o contexto. O pessoal da Gimlet acho que conseguiu fazê-lo de forma detalhada equilibrada (algo relativamente difícil de encontrar nos dias de hoje).

Mas gostava que se focassem na história do algoritmo do YouTube contada por [Kevin Roose](https://twitter.com/kevinroose?lang=pt).

O YouTube encontrou o algoritmo certo para manter os seus utilizadores presos num mundo de sugestões. Esse algoritmo promove ideias radicais e de extrema-direita. Deu um forte contributo à Alt-Right, mas também aos movimentos que defendem o fim das vacinas, da terra plana e outras que tais.

Quando chamados à responsabilidade perante uma clara violação dos termos e condições do seu serviço, o YouTube assobia para o lado. Porque para o YouTube não há essa coisa chamada “má clientela”.

Desde os tempos do Ray William Johnson que fui acumulando criadores de conteúdo que sigo religiosamente neste site. Aprendi imensa coisa, descobri bandas incríveis, ri, comovi-me e revoltei-me perante o site que começou com [um tipo no Zoo](https://www.youtube.com/watch?v=jNQXAC9IVRw). O YouTube foi, para este jovem nerd, a sua TV.

Mas depois de ouvir a [resposta oficial do YouTube](https://www.theverge.com/2019/6/4/18653088/youtube-steven-crowder-carlos-maza-harassment-bullying-enforcement-verdict), só consigo pensar o quanto gostaria de ter um outro site que não fizesse estas manobras contorcionistas.

Nota 1: O YouTube [anunciou](https://www.theverge.com/2019/7/11/20691123/youtube-creator-harassment-vidcon-carlos-maza-steven-crowder-neal-mohan) no dia 11 deste mês que estava a trabalhar em regras para evitar ‘creator-on-creator harassment’. Mas depois de quase um mês a dizer tudo e o seu contrário, vou esperar para ver.

Nota 2: Não, isto não é uma questão de liberdade de expressão. É uma questão de termos e condições e regras que nos foram apresentadas por uma empresa privada quando aceitamos fazer parte da sua comunidade mas que, agora, porque não lhe convém, não quer fazer valer esses termos e condições.







Kevin Roose, I’m a tech columnist for the NYT
